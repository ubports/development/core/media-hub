/*
 * Copyright © 2014 Canonical Ltd.
 * Copyright © 2022 UBports Foundation.
 *
 * Contact: Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Thomas Voß <thomas.voss@canonical.com>
 */

#ifndef LOMIRI_MEDIAHUBSERVICE_HYBRIS_RECORDER_OBSERVER_H
#define LOMIRI_MEDIAHUBSERVICE_HYBRIS_RECORDER_OBSERVER_H

#include "recorder_observer_p.h"

#include <QScopedPointer>

namespace lomiri
{
namespace MediaHubService
{
class HybrisRecorderObserver : public RecorderObserverPrivate
{
public:
    HybrisRecorderObserver(RecorderObserver *q);
    ~HybrisRecorderObserver();

private:
    struct Private;
    QScopedPointer<Private> d;
};
}
}

#endif // LOMIRI_MEDIAHUBSERVICE_HYBRIS_RECORDER_OBSERVER_H
